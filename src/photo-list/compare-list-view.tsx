import { ImageList } from "./photo-list-view";

interface Props {
  compareList: ImageList[];
}

const CompareListView = ({ compareList }: Props) => {
  return (
    <div className={"my-3 white-bg"}>
      <table className="table table-bordered">
        <thead>
          <tr>
            <th colSpan={4} className={"text-center"}>
              Comparsion Table
            </th>
          </tr>
          <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">URL</th>
            <th scope="col">Title</th>
          </tr>
        </thead>
        <tbody>
          {compareList && compareList.length
            ? compareList.map((item, index) => {
                return (
                  <tr key={index}>
                    <td className={"text-center"}>
                      <img
                        src={item.url}
                        alt={item.title}
                        className={"photo-img"}
                      />
                    </td>
                    <td  className={"vertical-align-middle"}>{item.id}</td>
                    <td  className={"vertical-align-middle"}>
                      <span className={'fw-bold px-2'}>Photo URL:</span>
                      <a href={`${item.url}`} target='_blank' rel="noreferrer">{`${item.url}`}</a>
                      <br />
                      <span  className={'fw-bold px-2'} >Photo Thumbnail URL:</span>
                      <a href={`${item.url}`} target='_blank' rel="noreferrer">{`${item.thumbnailUrl}`}</a>
                    </td>
                    <td  className={"vertical-align-middle"}>{item.title}</td>
                  </tr>
                );
              })
            : ""}
        </tbody>
      </table>
    </div>
  );
};
export default CompareListView;
