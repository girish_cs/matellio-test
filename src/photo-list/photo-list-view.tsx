import { useCallback, useEffect, useState } from "react";
import { getRequest } from "../utils/http-helper";
import "./photo-list.css";
import Loader from "../images/loader.gif";
import CompareListView from "./compare-list-view";

export interface ImageList {
  albumId: number;
  id: number;
  title: string;
  url: string;
  isCompare: boolean;
  thumbnailUrl: string;
}
const PhotoListView = () => {
  const [imageList, setImageList] = useState<ImageList[]>([]);
  const [compareList, setCompareList] = useState<ImageList[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const getImageList = useCallback(async () => {
    setLoading(true);
    let response = await getRequest("photos");
    if (response && response.data) {
      setImageList(
        response.data
          .slice(0, 12)
          .map((item: ImageList) => ({ ...item, isCompare: true }))
      );
    }
    setLoading(false);
  }, []);

  const onHandleClick = (imgList: ImageList) => {
    if (imgList.isCompare) {
      setImageList(
        imageList.map((item) => {
          if (item.id === imgList.id) {
            return { ...item, isCompare: false };
          }
          return item;
        })
      );
      setCompareList([...compareList, imgList]);
    } else {
      setCompareList(compareList.filter((i) => i.id !== imgList.id));
      setImageList(
        imageList.map((item) => {
          if (item.id === imgList.id) {
            return { ...item, isCompare: true };
          }
          return item;
        })
      );
    }
  };

  useEffect(() => {
    getImageList();
  }, [getImageList]);
  return imageList && imageList.length ? (
    <>
      <div>
        {loading ? <img src={Loader} className="loader" alt={"loader"} /> : ""}
      </div>

      <div className={`row text-center ${loading ? "loading" : ""}`}>
        <h2 className={"text-center my-4 fw-bold"}>Photo Listing View</h2>
        {imageList.map((item, index) => {
          return (
            <div key={index} className={"col-sm-4 pb-4"}>
              <div className={"box-modal p-3 white-bg"}>
                <img
                  src={item.url}
                  alt={item.title}
                  className={"photo-img pb-2"}
                />
                <p>
                  <span className={"title-info"}>Title:</span>
                  {`${item.title}`}
                </p>
                <p>
                  <span className={"title-info"}>ID:</span>
                  {`${item.id}`}
                </p>
                <p>
                  <span className={"title-info"}>URL:</span>
                  <a href={`${item.url}`} target='_blank' rel="noreferrer">{`${item.url}`}</a>
                </p>
                <button
                  onClick={() => {
                    onHandleClick(item);
                  }}
                  className={`compare-btn ${item.isCompare ? "green-bg" : "red-bg"}`}
                >
                  {item.isCompare ? "Compare" : "Remove"}
                </button>
              </div>
            </div>
          );
        })}
      </div>
      <CompareListView compareList={compareList} />
    </>
  ) : (
    <></>
  );
};
export default PhotoListView;
