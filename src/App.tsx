
import './App.css';
import PhotoListView from './photo-list/photo-list-view';
const App=()=> {
  return (
    <div className={'container-fluid'}>
      <PhotoListView />
    </div>
  );
}

export default App;
