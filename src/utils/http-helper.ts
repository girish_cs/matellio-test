import axios from "axios";
const BASE_URL = "https://jsonplaceholder.typicode.com/";

export const getRequest = (url: string, params?: any) => {
  url = BASE_URL + url;
  const options = {
    url,
    method: "get",
    ...params,
  };
  return axios(options);
};
